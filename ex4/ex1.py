from torch.utils.data import IterableDataset
import networkx as nx
import random as rd

import pickle

with open('')
class pq_walks(IterableDataset):

    def __init__(self, G, p, q, l, ln):
        super(pq_walks, self).__init__()
        self.p = p
        self.q = q
        self.l = l
        self.ln = ln
        self.s = rd.sample(G.nodes, 1)
        self.v = rd.sample(nx.neighbors(G,self.s))

pq_walks()