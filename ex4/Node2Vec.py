import torch
import numpy as np
from ex1_proposal import pq_walks
from torch.utils.data import IterableDataset, get_worker_info, DataLoader
import pickle

path='Citeseer/data.pkl'
with open(path,'rb') as f:
    data=pickle.load(f)

class node2vec(torch.nn.Module):

    def __init__(self,G):
        self.X=torch.nn.parameter(torch.zeros(len(self.G),128))
        torch.nn.init.kaiming_normal_(self.X)
        self.softmax=torch.nn.Softmax(dim=0)

    def forward(self,s,w,neg):
        temp=[np.dot(self.X[s],self.X[i]) for i in w+neg]
        temp=self.softmax(temp)
        loss=1
        for i in range(len(w)):
            loss*=temp[i]
        return -np.log(loss)

#test functionality
node2vec
train_loader = DataLoader(pq_walks(data[0], 1,2,5,5), batch_size=64)
for s, walk, neg_samples in train_loader:
