import torch
from torch_scatter import scatter_sum
import pickle

from customDataset import CustomDataset
from collate_graphs import collate_graphs


class Virtual_Node(torch.nn.Module):

    def __init__(self, hidden_dim):
        """
        Initializes a Virtual Node.

        :hidden_dim: hidden dimension of previous layer
        """
        super(Virtual_Node, self).__init__()

        self.W = torch.nn.Parameter(torch.zeros(hidden_dim, hidden_dim))
        torch.nn.init.kaiming_normal_(self.W)

        self.linear = torch.nn.Linear(hidden_dim, hidden_dim)

        # self.norm = torch.nn.BatchNorm1d(hidden_dim)

        

    def forward(self, H, batch_idx):
        """
        Forward pass for Sum Pooling.

        :H: vertex embedding of last layer
        :return: graph embeddings
        """

        #y = list()
        

       # for i in range(H.size(0)):
       #     W[i] = torch.nn.init.kaiming_normal_(self.W)
       #     y = scatter_sum(H, batch_idx, dim=0) #|G| x hidden dim
       #     y[i] = torch.matmul(y[i], self.W)
       #     y[i] = self.norm(y[i])
       #     y[i] = torch.relu(y[i])

        #self.W.add(W[i])
        #y.add(y[i])
        #y = H.add(y[batch_idx])

        y = scatter_sum(H, batch_idx, dim=0) #|G| x hidden dim
        #y = torch.matmul(y, self.W) |G| x hidden_dim
        y = self.linear(y)
        # y = self.norm(y)
        # apply activation
        y = torch.relu(y)
        #print("H:", H.size(), "y[batch_idx]:", y[batch_idx].size())
        # add information to each row of H (different information for each graph)
        y = H + y[batch_idx]
        
        return y




#test functionality
#data = pickle.load(open("../datasets/ZINC_Test/data.pkl", "rb"))
#dataset = CustomDataset(data)
#first_graphs = collate_graphs(dataset[:4])
#H = first_graphs[1]
#batch_idx = first_graphs[4]
#print(batch_idx.type())
#print("H", H.size(1))
#vn = Virtual_Node(H.size(1))
#print(vn(H, batch_idx))


