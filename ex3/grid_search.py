from Train_GNN import train_GNN
import itertools

hidden_dim=[50] #between 50 and 500
aggr_type=['sum'] #max, sum
num_layers=[7] #between 3 and 10
drop_out=[0.3] #between 0 and 0.5
virtual_node=[False]
epochs=[200]
batch_size=[100]
lr=[0.004]

train_data = "ZINC_Train/data.pkl"
validation_data = "ZINC_Val/data.pkl"
test_data = "ZINC_Test/data.pkl"

for x in itertools.product(hidden_dim, aggr_type, num_layers,drop_out,virtual_node,epochs, batch_size, lr):
    hidden_dim_x, aggr_type_x, num_layers_x, drop_out_x, virtual_node_x, epochs_x, batch_size_x, lr_x = x
    model_name=f'\nhidden_dim:{hidden_dim_x},\naggr_type: {aggr_type_x}, \nnum_layers:{num_layers_x}, \ndrop_out:{drop_out_x}' \
               f',\nvirtual_node: {virtual_node_x}, \nepochs:{epochs_x}, \nbatch_size:{batch_size_x}, \nlr:{lr_x}'
    train_GNN(train_data,validation_data, test_data,hidden_dim_x, aggr_type_x, num_layers_x, drop_out_x, virtual_node_x, epochs_x, batch_size_x, lr_x)



