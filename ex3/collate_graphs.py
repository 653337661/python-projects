from numpy import int64
from customDataset import CustomDataset
import pickle
import torch


def collate_graphs(data):
    #initialization of the tensors which should be returned in the end
    edge_list = torch.tensor([]).long()
    node_features = torch.tensor([])
    edge_features = torch.tensor([])
    graph_label = torch.tensor([])
    batch_idx = torch.tensor([], dtype=torch.int64)
    n_nodes = 0

    #go through each graph in data and concatenate the graph info to the final tensors
    for i, graph in enumerate(data):
        i_edge_list, i_node_features, i_edge_features, i_graph_label = graph
        #print(i_edge_list.size(), i_node_features.size(), i_edge_features.size(), i_graph_label.size())
        
        #add the number of nodes which have been seen before to the edge list and concatenate afterwards
        edge_list = torch.cat((edge_list, torch.add(i_edge_list, n_nodes)), dim=1)
        node_features = torch.cat((node_features, i_node_features), dim=0)
        edge_features = torch.cat((edge_features, i_edge_features), dim=0)
        graph_label = torch.cat((graph_label, i_graph_label))
        n_nodes += i_node_features.size(0)
        #add the number of the graph n_nodes times to the batch_idx, once for each node
        batch_idx = torch.cat((batch_idx, torch.tensor([i]*i_node_features.size(0), dtype=torch.int64)))
             
    return (edge_list, node_features, edge_features, graph_label, batch_idx)




#test functionality
# data = pickle.load(open("ZINC_Test/data.pkl", "rb"))
# dataset = CustomDataset(data)
# print(collate_graphs(dataset[:4]))