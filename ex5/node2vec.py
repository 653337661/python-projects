import torch
import networkx as nx
import pickle
import numpy as np

with open("datasets/CITE/data.pkl", "rb") as f:
    CITE = pickle.load(f)

node_attrs = nx.get_node_attributes(CITE,'node_attributes')
node_attrs = list(node_attrs.values())
features = np.array(node_attrs)

node_labels = nx.get_node_attributes(CITE,'node_label')
node_labels = list(node_labels.values())
labels = np.array(node_labels)


class node2vec(torch.nn.Module):
    def __init__(self,input_dim):
        super(node2vec, self).__init__()
        self.W=torch.nn.Parameter(torch.zeros(input_dim,features.shape[1]))
        torch.nn.init.kaiming_normal_(self.W)
        # self.softmax=torch.nn.Softmax(dim=0)

    def forward(self,x):
        # torch.matmul(self.W,x)
        return torch.matmul(self.W,x)