import pickle
import numpy as np

with open('datasets/CITE/data.pkl','rb') as f:
    data = pickle.load(f)


# y_label = [np.int32([node[1]["node_label"] for node in data.nodes(data=True)])]
#
# for node in data.nodes(data=True):
#     print(node[1]['node_label'])
test = [node[1]["node_label"] for node in data.nodes(data=True)]
# print(y_label)
y_label = np.int32(test)
# print(type(test))